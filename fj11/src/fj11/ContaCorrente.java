package fj11;

public class ContaCorrente extends Conta implements Autenticador{

	public ContaCorrente(String agencia, String numero, Integer senha, Cliente cliente, Double valor) {
		super(agencia, numero, senha, cliente, valor);
	}


	@Override
	public void saca(Double valor) {
		if(valor >= 1000.0) {
			valor += 100;
			this.valor -= valor;
		}else {
			valor += 50;
			this.valor -= valor;
		}
	}

	@Override
	public void autentica(Integer senha) {

		if(getSenha() == null) {
			
			System.out.println("ERRADO!");
			
		}else {
			System.out.println("CERTO!");
		}
	}
	
	


}
