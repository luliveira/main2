package fj11;

public class TesteClienteComConta {
	
	public static void main(String[] args) {
		
		
		Cliente cliente = new Cliente();
		cliente.setNome("Lucas");
		
		ContaCorrente conta = new ContaCorrente("123", "321", null, cliente, 1000.0);
		
		String nome = conta.getTitular().getNome();
		System.out.println(nome);		
		
		
		conta.autentica(conta.getSenha());
		System.out.println("AUTENTICADA!");
		
		
	}

}
