package fj11;

public abstract class Conta {

	private String agencia;
	private String numero;
	private Integer senha;
	private Cliente titular;
	protected Double valor;

	public Conta(String agencia, String numero, Integer senha, Cliente titular, Double valor) {
		this.agencia = agencia;
		this.numero = numero;
		this.senha = senha;
		this.titular = titular;
		this.valor = valor;
	}

	public String getAgencia() {
		return agencia;
	}

	public void setAgencia(String agencia) {
		this.agencia = agencia;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public Integer getSenha() {
		return senha;
	}

	public void setSenha(Integer senha) {
		this.senha = senha;
	}

	public Cliente getTitular() {
		return titular;
	}

	public void setTitular(Cliente titular) {
		this.titular = titular;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}
	
	public void saca(Double valor) {
		this.valor -= valor;		
	}
	
	
	public void deposita(Integer valor) {
		this.valor += valor;
	}
	
	public void transfere (Integer valor, Conta conta) {
		this.valor -= valor;
		conta.setValor((conta.getValor() + valor));
	}



}
